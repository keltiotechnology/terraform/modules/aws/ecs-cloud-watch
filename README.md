# Cloudwatch events and alarms

## Usage

```hcl-terraform
locals {
  my_ecs_services_name = ["backend", "frontend"]
}
data "aws_ecs_cluster" "prod_cluster" {
  cluster_name = "my_cluster"
}
data "aws_ecs_task_definition" "swappy_prod_front_template" {
  task_definition = "my-frontend-template"
}
data "aws_ecs_task_definition" "swappy_prod_back_template" {
  task_definition = "my-backend-template"
}
module "ecs_cloudwatch_alerts" {
  source                          = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-ecs-cloudwatch-alerts"
  namespace                       = "devops"
  stage                           = "dev"

  cluster_arn                     = data.aws_ecs_cluster.prod_cluster.arn
  ecs_task_restart_service_name   = local.my_ecs_services_name
  ecs_task_restart_task_name      = [
    data.aws_ecs_task_definition.swappy_prod_front_template.family,
    data.aws_ecs_task_definition.swappy_prod_back_template.family
  ]
  ecs_task_definition_arn         = [
    "${data.aws_ecs_task_definition.swappy_prod_front_template.family}:${data.aws_ecs_task_definition.swappy_prod_front_template.revision}",
    "${data.aws_ecs_task_definition.swappy_prod_back_template.family}:${data.aws_ecs_task_definition.swappy_prod_back_template.revision}"
  ]

  sns_topic_ecs_state_change_arn  = module.sns_prod_state_change.aws_sns_topic_arn
  sns_topic_ecs_task_stopped_arn  = module.sns_prod_state_change.aws_sns_topic_arn
}
```

To subscribe to Alarm Task Restart, we need to provide a list of ARN SNS topic. Use the sample above example:

```hcl-terraform

# ...
# ...

module "sns_subscription_task_restart" {
  source                            = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-sns-email-subscription"
  sns_topic_name                    = "SNS_TOPIC_TASK_RESTART"
  user_emails                       = ["user@example.com"]
}

module "ecs_cloudwatch_alerts" {
  # ...
  # ...
  # Provide a list of SNS Topic ARNs
  task_restart_alarm_actions      = [module.sns_subscription_task_restart.aws_sns_topic_arn]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_event_rule.stage_change](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_rule.task_restart](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_rule.task_stopped](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_target.sns](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_cloudwatch_event_target.stage_change_sns_topic](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_cloudwatch_event_target.task_restart](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_cloudwatch_log_group.task_restart](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_log_metric_filter.task_restart_count](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_metric_filter) | resource |
| [aws_cloudwatch_metric_alarm.task_restart](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudwatch_state_changes_description"></a> [cloudwatch\_state\_changes\_description](#input\_cloudwatch\_state\_changes\_description) | Description of the Cloudwatch event rule when task/container state changes | `string` | `""` | no |
| <a name="input_cloudwatch_state_changes_event_rule_name"></a> [cloudwatch\_state\_changes\_event\_rule\_name](#input\_cloudwatch\_state\_changes\_event\_rule\_name) | Name of the Cloudwatch event rule when task/container state changes | `string` | `"ECS_StateChangeRules"` | no |
| <a name="input_cloudwatch_task_restart_event_rule_description"></a> [cloudwatch\_task\_restart\_event\_rule\_description](#input\_cloudwatch\_task\_restart\_event\_rule\_description) | Event rule to monitor ecs service tasks not at running status | `string` | `""` | no |
| <a name="input_cloudwatch_task_restart_event_rule_name"></a> [cloudwatch\_task\_restart\_event\_rule\_name](#input\_cloudwatch\_task\_restart\_event\_rule\_name) | Name of the Cloudwatch event rule to monitor ecs service tasks not at running status | `string` | `"ECS-Task-Restart"` | no |
| <a name="input_cloudwatch_task_stopped_event_rule_description"></a> [cloudwatch\_task\_stopped\_event\_rule\_description](#input\_cloudwatch\_task\_stopped\_event\_rule\_description) | Event rule to monitor ecs service tasks not at running status | `string` | `""` | no |
| <a name="input_cloudwatch_task_stopped_event_rule_name"></a> [cloudwatch\_task\_stopped\_event\_rule\_name](#input\_cloudwatch\_task\_stopped\_event\_rule\_name) | Name of the Cloudwatch event rule to monitor ecs service tasks not at running status | `string` | `"ECS-Task-Stopped"` | no |
| <a name="input_cluster_arn"></a> [cluster\_arn](#input\_cluster\_arn) | ARN of ECS cluster | `string` | n/a | yes |
| <a name="input_ecs_cloudwatch_task_restart_target_id_prefix"></a> [ecs\_cloudwatch\_task\_restart\_target\_id\_prefix](#input\_ecs\_cloudwatch\_task\_restart\_target\_id\_prefix) | Prefix for ecs cloudwath task restart target id | `string` | `"ECSTaskRestart"` | no |
| <a name="input_ecs_task_definition_arn"></a> [ecs\_task\_definition\_arn](#input\_ecs\_task\_definition\_arn) | A list of ARN of task definitions used by tasks running inside ECS cluster | `list(string)` | n/a | yes |
| <a name="input_ecs_task_restart_service_name"></a> [ecs\_task\_restart\_service\_name](#input\_ecs\_task\_restart\_service\_name) | A list of service names of the ECS sercices which have the task we need to monitor task restart event | `list(string)` | n/a | yes |
| <a name="input_ecs_task_restart_task_name"></a> [ecs\_task\_restart\_task\_name](#input\_ecs\_task\_restart\_task\_name) | A list of the ECS task names to monitor task restart event | `list(string)` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name | `string` | `""` | no |
| <a name="input_sns_topic_ecs_state_change_arn"></a> [sns\_topic\_ecs\_state\_change\_arn](#input\_sns\_topic\_ecs\_state\_change\_arn) | ARN of the SNS topic where ECS task/container state changes | `string` | n/a | yes |
| <a name="input_sns_topic_ecs_task_stopped_arn"></a> [sns\_topic\_ecs\_task\_stopped\_arn](#input\_sns\_topic\_ecs\_task\_stopped\_arn) | Name of SNS topic when ECS service tasks is not at running status | `string` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, which can be 'prod', 'staging', 'dev'... | `string` | `""` | no |
| <a name="input_task_restart_alarm_actions"></a> [task\_restart\_alarm\_actions](#input\_task\_restart\_alarm\_actions) | The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed. | `list(string)` | `[]` | no |
| <a name="input_task_restart_alarm_description"></a> [task\_restart\_alarm\_description](#input\_task\_restart\_alarm\_description) | Description of the Alarm to alert a task inside ECS cluster keeps restart more than X time | `string` | `"Alarm when the task keeps restart more than X time"` | no |
| <a name="input_task_restart_alarm_name"></a> [task\_restart\_alarm\_name](#input\_task\_restart\_alarm\_name) | Name of the Alarm to alert a task inside ECS cluster keeps restart more than X time | `string` | `"TaskRestart"` | no |
| <a name="input_task_restart_evaluation_periods"></a> [task\_restart\_evaluation\_periods](#input\_task\_restart\_evaluation\_periods) | Evaluation period for the alarm task restart | `number` | `3` | no |
| <a name="input_task_restart_log_group_retention_time"></a> [task\_restart\_log\_group\_retention\_time](#input\_task\_restart\_log\_group\_retention\_time) | Number of days you want to retain log events of task restart event in the specified log group | `number` | `7` | no |
| <a name="input_task_restart_metric_name"></a> [task\_restart\_metric\_name](#input\_task\_restart\_metric\_name) | Name of the metric name to record number of task restart event | `string` | `"TaskRestartEventCount"` | no |
| <a name="input_task_restart_metric_namespace"></a> [task\_restart\_metric\_namespace](#input\_task\_restart\_metric\_namespace) | Namespace name of the metric name to record number of task restart event | `string` | `"ECS/CustomEvent"` | no |
| <a name="input_task_restart_period"></a> [task\_restart\_period](#input\_task\_restart\_period) | Period of time to aggregate number of occured task events | `number` | `60` | no |
| <a name="input_task_restart_threshold"></a> [task\_restart\_threshold](#input\_task\_restart\_threshold) | Threshold to alert when number of task restart events is reaching | `number` | `5` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alarm_task_restart_arn"></a> [alarm\_task\_restart\_arn](#output\_alarm\_task\_restart\_arn) | A list of ARN of the cloudwatch metric alarms to alert when ecs service tasks keeps restarting more than a specified times |
| <a name="output_event_rule_state_change_arn"></a> [event\_rule\_state\_change\_arn](#output\_event\_rule\_state\_change\_arn) | ARN of the Cloudwatch Event Rule when task/container state changes |
| <a name="output_event_rule_stopped_service_tasks"></a> [event\_rule\_stopped\_service\_tasks](#output\_event\_rule\_stopped\_service\_tasks) | ARN of the cloudwatch metric alarm to alert when ecs service tasks is not at running status |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
